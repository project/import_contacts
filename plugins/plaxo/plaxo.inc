<?php


/**
 * Implementation of import_contacts_hook().
 */
function import_contacts_plaxo($username, $password) {
  $contacts = array();

  // login
  // TODO: Auto-detect all login form fields + values
  $html = dataminerapi_http('https://www.plaxo.com/signin', array(
    'r' => '/po3/',
    't' => 'corp',
    'originalEmail' => '',
    'signin' => 'true',
    'smi' => 0,
    'signin_method' => 'email',
    'signin.email' => $username,
    'signin.password' => $password,
  ), 'POST');

  // detect errors
  if (dataminerapi_detect_error($html, array(
    'is not linked to your Plaxo account and cannot be used to sign in.' => t('Invalid username and/or password.'),
    'You have entered an incorrect e-mail address or password.' => t('Invalid username and/or password.'),
  ))) { return; }

  // export contacts to CSV
  $html = dataminerapi_http('http://www.plaxo.com/export');
  $form = array_shift(dataminerapi_get_form_values($html));
  $csv = dataminerapi_http('http://www.plaxo.com/export/plaxo_ab_outlook.csv', array(
    'paths.0.folder_id' => $form['paths.0.folder_id']['value'],
    'paths.0.checked' => 'on',
    'NumPaths' => 1,
    'type' => 'O',
    'do_submit' => 1,
    'x' => rand(40, 50),
    'y' => rand(20, 30),
  ), 'POST');
  $rows = explode("\n", trim($csv));
  if (count($rows)) {
    $headers = array_splice($rows, 0, 1);
    foreach((array) $rows as $row) {
      $cols = explode(',', $row);
      // Title, First Name, Middle Name, Last Name, Suffix
      $full_name = trim($cols[0], '" ') .' '. trim($cols[1], '" ') .' '. trim($cols[2], '" ') .' '. trim($cols[3], '" ') .' '. trim($cols[4], '" ');
      // E-mail Address
      $email = trim($cols[5], '" ');
      if ($contact = _import_contacts_format_contact($full_name, $email)) { // verify and cleanup
        $contacts[] = $contact; // append
      }
    }
  }

  return $contacts;
}

// Register plugin with Import Contacts
import_contacts_register_plugin('Plaxo', 'Plaxo.com');