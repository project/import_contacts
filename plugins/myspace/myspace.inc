<?php


/**
 * Implementation of import_contacts_hook().
 */
function import_contacts_myspace($username, $password) {
  $contacts = array();

  // get session cookie + login form
  $html = dataminerapi_http('http://www.myspace.com');
  $s = dataminerapi_simplexml($html, TRUE);

  // get form action, email, and password field names
  if (
    ($action = dataminerapi_get_attr($s, "//form//input[contains(@name, 'Password')]/ancestor::form", 'action', NULL)) &&
    ($email_name = dataminerapi_get_attr($s, "//form//input[contains(@name, 'Email')]", 'name', NULL)) &&
    ($password_name = dataminerapi_get_attr($s, "//form//input[contains(@name, 'Password')]", 'name', NULL))
  ) {
    // submit login information
    $html = dataminerapi_http($action, array(
      $email_name => $username,
      $password_name => $password,
    ), 'POST');

    // detect errors
    if (dataminerapi_detect_error($html, array(
      'Sorry! an unexpected error has occurred.' => t('Invalid username and/or password.')
    ))) { return; }

    // scrape email addresses from address book
    $html = dataminerapi_http('http://addressbook.myspace.com/index.cfm?fuseaction=adb');
    $s = dataminerapi_simplexml($html, TRUE);
    $pages = (int) dataminerapi_get_val($s, "//td[contains(text(), 'Listing')]/following-sibling::td/a[last()]");
    for ($page=0; $page<$pages;) {
      $rows = dataminerapi_xpath($s, "//table[@id='addresses']/tbody/tr");
      foreach ((array) $rows as $row) {
        $full_name = trim(dataminerapi_get_val($row, "./td[2]/a"));
        $email = dataminerapi_innerText(array_shift(dataminerapi_xpath($row, "./td[3]")));
        if ($contact = _import_contacts_format_contact($full_name, $email)) { // verify and cleanup
          $contacts[] = $contact; // append
        }
      }
      unset($row, $rows, $first_last, $email);

      // move to next page
      $html = dataminerapi_http('http://addressbook.myspace.com/index.cfm?fuseaction=adb', array(
        'keywords' => '',
        'GroupID' => 0,
        'StartChar' => '',
        'page' => ++$page,
      ), 'POST');
      $s = dataminerapi_simplexml($html, TRUE);
    }
  }

  return $contacts;
}

// Register plugin with Import Contacts
import_contacts_register_plugin('MySpace', 'MySpace.com');