<?php


/**
 * Implementation of import_contacts_hook().
 */
function import_contacts_multiply($username, $password) {
  $contacts = array();

  // get session cookie
  $html = dataminerapi_http('http://multiply.com/', array(
    'form::signin' => 'signin',
    'form::signin::count' => '1',
    'signin::xurl' => '',
    'signin::remember_me_for' => '604800000',
    'signin::id' => $username,
    'signin::password' => $password,
    'omniture_submission' => 'submitted',
  ), 'POST');

  // detect errors
  if (dataminerapi_detect_error($html, array(
    'The User ID and Password combination you entered is incorrect.' => t('Invalid username and/or password.'),
  ))) { return; }

  // scrape friend emails
  $csv = dataminerapi_http('http://multiply.com/network/contacts.csv');
  $rows = explode("\n", trim($csv));
  if (count($rows)) {
    $headers = array_splice($rows, 0, 1);
    foreach((array) $rows as $row) {
      $cols = explode(',', $row);
      $full_name = trim($cols[0], '" ') .' '. trim($cols[1], '" ');
      $email = trim($cols[2], '" ');
      if ($contact = _import_contacts_format_contact($full_name, $email)) { // verify and cleanup
        $contacts[] = $contact; // append
      }
    }
  }

  return $contacts;
}

// Register plugin with Import Contacts
import_contacts_register_plugin('Multiply', 'Multiply.com');