<?php


/**
 * Implementation of import_contacts_hook().
 */
function import_contacts_flickr($username, $password) {
  // get session cookie
  $html = dataminerapi_http('http://www.flickr.com/signin');
  exit;

  preg_match('|location\: (https\:\/\/login\.yahoo\.com\/config\/login.*)|',$result,$loc);
  $result = get_curl($loc[1],$cookie_file_path,"","",true);

//  $inputs=get_hidden($result);
//  $hiddens=use_hidden($inputs);
  $url="https://login.yahoo.com/config/login?";
  $POSTFIELDS = 'login='.urlencode($_POST['username']).'&passwd='.urlencode($_POST['password']).$hiddens.'&.save=Sign+In';
  $result1= get_curl($url,$cookie_file_path,$POSTFIELDS,"",true);

  preg_match('|Location\: (https\:\/\/login\.yahoo\.com\/config\/validate.*)|',$result1,$loc);
  $result = get_curl($loc[1],$cookie_file_path,"","",true);

  preg_match("|window\.location\.replace\(\"(.*)\"\);|",$result,$loc);
  $result = get_curl($loc[1],$cookie_file_path,"","",true);

  if (eregi('Invalid ID or password.', $result1))
  {
    return "Invalid ID or password.";
  }
  elseif (eregi('This ID is not yet taken.', $result1))
  {
    return "This ID is not yet taken.";
  }
  else
  {
    return "true";
  }

  // get friend emails

  global $cookie_file_path, $cookie;
  $result = get_curl("http://www.flickr.com/messages_write.gne",$cookie_file_path,"","",true);

  preg_match_all("/<option value=\"(.*)\"\s+>(.*)<\/option>/i",$result,$friends);
  $contacts = array();
  for($i=0;$i<(count($friends[1]));$i++)
  {
    $contacts[] = array('name' => $friends[2][$i], 'id' => $friends[1][$i]);
  }
  return $contacts;
}

// TODO: There's no way to scrape user email addresses on Flickr.
//       However, it would be possible to send FlickrMail to each friend.
//       I'll need to build a custom _invite() hook for each plugin like this...

// Register plugin with Import Contacts
//import_contacts_register_plugin('Flickr', 'Flickr.com');