<?php


/**
 * Implementation of import_contacts_hook().
 */
function import_contacts_bebo($username, $password) {
  // get session cookie
  $html = dataminerapi_http('http://www.bebo.com/SignIn.jsp');

  // login
  $html = dataminerapi_http('http://www.bebo.com/SignIn.jsp', array(
    'FriendsMemberId' => '',
    'FriendsChecksumNbr' => '',
    'InviteRecipientId' => '',
    'InviteChecksumNbr' => '',
    'Page' => 'Home_jsp',
    'QueryString' => '',
    'EmailUsername' => $username,
    'Password' => $password,
    'SignIn' => 'Sign In >',
  ), 'POST');

  // detect errors
  if (dataminerapi_detect_error($html, array(
    'Your password is incorrect, please try again' => t('Invalid username and/or password.'),
    'has not been recognized. Please check and try again.' => t('Invalid username and/or password.'),
  ))) { return; }

  // scrape contacts
  $contacts = array();
  $html = dataminerapi_http('http://www.bebo.com/MyFriends.jsp');
  $part = explode("<td bgcolor=#efefef width=152 valign=top><table width='100%'><tr><td class=m_c>", $html);
  for ($i=0;$i<count($part);$i++) {
    if (eregi("Profile\.jsp\?MemberId=", $part[$i])) { // find challenge field
      preg_match_all("|<a href=Profile\.jsp\?MemberId=.*><img src=(.*) width=90 height=90 border=0\/><\/a><\/td><td valign=top align=right><a href=ContactDelete.jsp\?MemberId=.*><img src=http\:\/\/s.bebo.com\/img\/delete\_icon\.gif border=0 width=22 height=22 alt='Delete Friend'><\/a><tr><td align=center colspan=2><a href=Profile.jsp\?MemberId=(.*)>(.*)<\/a><\/td>|",$part[$i],$friends, PREG_PATTERN_ORDER);
      if($friends[3][0]<>"") {
        $contacts[] = array('name' => $friends[3][0], 'id' => $friends[2][0], 'image' => $friends[1][0]);
      }
    }
  }
  return $contacts;
}

// TODO: There's no way to scrape user email addresses on Bebo.
//       However, it would be possible to send a message to each friend via Bebo.
//       I'll need to build a custom _invite() hook for each plugin like this...

// Register plugin with Import Contacts
//import_contacts_register_plugin('Bebo', 'Bebo.com');