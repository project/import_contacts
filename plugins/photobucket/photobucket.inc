<?php


/**
 * Implementation of import_contacts_hook().
 */
function import_contacts_photobucket($username, $password) {
  $contacts = array();

  // get session cookie + login form
  $html = dataminerapi_http('http://photobucket.com');

  // login
  $html = dataminerapi_http('http://photobucket.com/login', array(
    'action' => 'login',
    'redir' => '',
    'redirectAction' => '',
    'redirectUrl' => '',
    'usernameemail' => $username,
    'password' => $password,
    'login' => 'Login',
  ), 'POST');

  // detect errors
  if (dataminerapi_detect_error($html, array(
    'Incorrect username or password' => t('Invalid username and/or password.')
  ))) { return; }

  // scrape email addresses from contact list
  $base_href = dataminerapi_get_attr($html, "//a[contains(text(), 'welcome')]", 'href', '');
  $html = dataminerapi_http($base_href . '?action=viewcontacts');
  $rows = dataminerapi_xpath($html, "//table[@id='contactsList']/tbody/tr");
  foreach ((array) $rows as $row) {
    $first_last = dataminerapi_get_val($row, "./td[1]/label");
    $email = dataminerapi_get_val($row, "./td[2]/label");
    $contacts[] = array('name' => $first_last, 'email' => $email);
  }

  return $contacts;
}

// Register plugin with Import Contacts
import_contacts_register_plugin('Photobucket', 'Photobucket.com');