<?php

?>
<br /><br /><hr />
<div id="address_book_upload">
  <h4>Email Application <span style="color: rgb(119, 119, 119); font-weight: normal; font-size: 11px;">Outlook, Apple Mail, etc.</span></h4>
  <div id="contacts_file_import">
    <h2>Heading text, Microsoft Outlook Express, Thunderbird and others</h2>
    <div class="instructions">Upload a contact file and we will tell you which of your contacts are on Facebook.<br>
      <a href="#" onclick="$('#contacts_file_help').toggle(); return false;">How to create a contact file...</a>.
      <div class="file_help" id="contacts_file_help" name="contacts_file_help" style="display: none;">
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_outlook').toggle(); return false;">Outlook</a></div>
          <div class="fh_instr" id="fh_outlook" name="fh_outlook" style="display: none;">To export a <acronym title="Comma-Separated Values">CSV</acronym> or tab-delimited text file from Outlook:
            <ol class="fh_steps">
              <li>Open Outlook</li>
              <li>Select "Import and Export" from the File menu</li>
              <li>When the wizard opens, select "Export to a file" and click "Next"</li>
              <li>Select "Comma separated values (Windows)" and click "Next"</li>
              <li>Select the Contacts folder you would like to export and click "Next"</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.csv" on the Desktop), then click "Next"</li>
              <li>Confirm what you are exporting: make sure the checkbox next to "Export..."  is checked and click "Finish"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_outlook_express').toggle(); return false;">Outlook Express</a></div>
          <div class="fh_instr" id="fh_outlook_express" name="fh_outlook_express" style="display: none;">To export a <acronym title="Comma-Separated Values">CSV</acronym> or tab-delimited text file from Outlook Express:
            <ol class="fh_steps">
              <li>Open Outlook Express</li>
              <li>Select "Export" from the "File" menu</li>
              <li>Click "Export", and then click "Address Book"</li>
              <li>Select "Text File" (Comma Separated Values), and then click "Export".</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.csv" on the Desktop) and click "Next"</li>
              <li>Click to select the check boxes for the fields that you want to export, and then click "Finish". Please be sure to select the email address and name fields</li>
              <li>Click "OK" and then click "Close"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_win_ab').toggle(); return false;">Windows Address Book</a></div>
          <div class="fh_instr" id="fh_win_ab" name="fh_win_ab" style="display: none;">To export a <acronym title="Comma-Separated Values">CSV</acronym> file from Windows Address Book:
            <ol class="fh_steps">
              <li>Open Windows Address Book</li>
              <li>From the "File" menu, select "Export", then "Other Address Book..."</li>
              <li>When the "Address Book Export Tool" dialog opens, select "Text File (Comma Separated Values)" and click "Export"</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.csv" on the Desktop) and click "Next"</li>
              <li>Click to select the check boxes for the fields that you want to export, and then click "Finish". Please be sure to select the email address and name fields</li>
              <li>Click "OK" and then click "Close"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_thunder').toggle(); return false;">Mozilla Thunderbird</a></div>
          <div class="fh_instr" id="fh_thunder" name="fh_thunder" style="display: none;">To export an LDIF file from Mozilla Thunderbird:
            <ol class="fh_steps">
              <li>Open Mozilla Thunderbird</li>
              <li>Select "Address Book" from the "Tools" menu</li>
              <li>When the address book window opens, select "Export..." from the "Tools" menu</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.ldif" on the Desktop) and click "Save"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_palm_desk_del').toggle(); return false;">Palm Desktop</a></div>
          <div class="fh_instr" id="fh_palm_desk_del" name="fh_palm_desk_del" style="display: none;">To export a <acronym title="Comma-Separated Values">CSV</acronym> file from Palm Desktop:
            <ol class="fh_steps">
              <li>Open Palm Desktop</li>
              <li>Display your contact list by clicking the "Addresses" icon on the lefthand side of the screen</li>
              <li>Select "Export" from the "File" menu</li>
              <li>When the dialog box opens:
                <ul>
                  <li>Enter a name for the file you are creating in the "File name:" field</li>
                  <li>Select "Comma Separated" in the "Export Type" pulldown menu</li>
                  <li>Be sure to select the "All" radio button from the two "Range:" radio buttons</li>
                </ul>
              </li>
              <li>A second dialog box "Specify Export Fields" opens. Leave all of the checkboxes checked, and click "OK"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_palm_desk_vcf').toggle(); return false;">Palm Desktop (vCard)</a></div>
          <div class="fh_instr" id="fh_palm_desk_vcf" name="fh_palm_desk_vcf" style="display: none;">To export a vCard file from Palm Desktop:
            <ol class="fh_steps">
              <li>Open Palm Desktop</li>
              <li>Display your contact list by clicking the "Addresses" icon on the left hand side of the screen</li>
              <li>Select the contacts you would like to export</li>
              <li>Select "Export vCard..." from the "File" menu</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.vcf" on the Desktop), then click "Save"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_entourage').toggle(); return false;">Entourage</a></div>
          <div class="fh_instr" id="fh_entourage" name="fh_entourage" style="display: none;">To export a tab-delimited text file from Entourage:
            <ol class="fh_steps">
              <li>Open Entourage</li>
              <li>Select "Export" from the "File" menu</li>
              <li>Select "Local Contacts to a list (tab-delimited text)"</li>
              <li>Click the "Next" arrow</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.txt" on the Desktop), then click "Save"</li>
              <li>Click "Done" in the confirm dialog</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_osx_addr_bk').toggle(); return false;">Mac OS X Address Book</a></div>
          <div class="fh_instr" id="fh_osx_addr_bk" name="fh_osx_addr_bk" style="display: none;">To export a vCard file from Mac OS X Address Book:
            <ol class="fh_steps">
              <li>Open Mac OS X Address Book</li>
              <li>Select the contacts you would like to export</li>
              <li>Select "Export vCards..." from the File menu</li>
              <li>Choose a filename and a place to save the file (for instance, "Contacts.vcf" on the Desktop), then click "Save"</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_linkedin').toggle(); return false;">LinkedIn</a></div>
          <div class="fh_instr" id="fh_linkedin" name="fh_linkedin" style="display: none;">To export a <acronym title="Comma-Separated Values">CSV</acronym> file from LinkedIn:
            <ol class="fh_steps">
              <li>Sign into LinkedIn</li>
              <li>Visit the <a href="http://www.linkedin.com/addressBookExport">Address Book Export</a> page</li>
              <li>Select "Microsoft Outlook (.CSV file)" and click "Export"</li>
              <li>Choose a filename and a place to save the exported file (for instance, "Contacts.csv" on the Desktop)</li>
            </ol>
            <p>To upload your file to Facebook, simply hit "Browse" or "Choose File" below and select the contact file you've just created.</p>
          </div>
        </div>
        <div class="fh_sect">
          <div class="fh_title"><a href="#" onclick="$('#fh_other').toggle(); return false;">Other</a></div>
          <div class="fh_instr" id="fh_other" name="fh_other" style="display: none;">Many email clients and <acronym title="Personal Information Management">PIM</acronym> programs allow contacts to be exported to a file. We support the following types of contact file:
            <ul>
              <li>Comma-separated values (.csv)</li>
              <li>vCards (.vcf)</li>
              <li>Tab-delimited text (.txt)</li>
              <li><acronym title="Lightweight Directory Access Protocol">LDAP</acronym> Data Interchange Format (.ldif)</li>
            </ul>
            Please check your email client's documentation to see whether contact export to one of these formats is supported. If it's not, you can <a href="help.php?tab=suggest">make a suggestion</a> for us to add support for the contact file format that your email client is capable of exporting.</div>
        </div>
      </div>
    </div>
  </div>
</div>